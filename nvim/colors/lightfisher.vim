" kingfisher.vim -- Vim color scheme.
" Author:      Fabian Neuschmidt (fabian@neuschmidt.de)
" Webpage:     http://www.example.com
" Description: A minimal teal colorscheme for (neo)vim

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "kingfisher"

if ($TERM =~ '256' || &t_Co >= 256) || has("gui_running")
    hi Cursor ctermbg=131 ctermfg=59 cterm=NONE guibg=#bf4130 guifg=#344246 gui=NONE
    hi CursorLine ctermbg=188 ctermfg=NONE cterm=NONE guibg=#cedbde guifg=NONE gui=NONE
    hi CursorColumn ctermbg=188 ctermfg=NONE cterm=NONE guibg=#cedbde guifg=NONE gui=NONE
    hi ColorColumn ctermbg=NONE ctermfg=131 cterm=NONE guibg=NONE guifg=#bf4130 gui=NONE
    hi StatusLine ctermbg=188 ctermfg=59 cterm=bold guibg=#cedbde guifg=#4f6166 gui=bold
    hi StatusLineNC ctermbg=188 ctermfg=66 cterm=NONE guibg=#cedbde guifg=#65737e gui=NONE
    hi StatusLineTerm ctermbg=188 ctermfg=59 cterm=bold guibg=#cedbde guifg=#4f6166 gui=bold
    hi StatusLineTermNC ctermbg=188 ctermfg=66 cterm=NONE guibg=#cedbde guifg=#65737e gui=NONE
    hi LineNr ctermbg=188 ctermfg=145 cterm=NONE guibg=#d9e6e9 guifg=#a7b6ba gui=NONE
    hi CursorLineNr ctermbg=188 ctermfg=66 cterm=bold guibg=#cedbde guifg=#65737e gui=bold
    hi VertSplit ctermbg=188 ctermfg=145 cterm=NONE guibg=#d9e6e9 guifg=#a7b6ba gui=NONE
    hi TabLine ctermbg=188 ctermfg=66 cterm=NONE guibg=#cedbde guifg=#65737e gui=NONE
    hi TabLineFill ctermbg=188 ctermfg=66 cterm=NONE guibg=#cedbde guifg=#65737e gui=NONE
    hi TabLineSel ctermbg=188 ctermfg=59 cterm=bold guibg=#cedbde guifg=#4f6166 gui=bold
    hi Title ctermbg=NONE ctermfg=66 cterm=NONE guibg=NONE guifg=#65737e gui=NONE
    hi Pmenu ctermbg=188 ctermfg=59 cterm=NONE guibg=#cedbde guifg=#4f6166 gui=NONE
    hi PmenuSbar ctermbg=152 ctermfg=152 cterm=NONE guibg=#c1ccce guifg=#c1ccce gui=NONE
    hi PmenuSel ctermbg=31 ctermfg=16 cterm=NONE guibg=#2d98be guifg=#0d1a1e gui=NONE
    hi PmenuThumb ctermbg=66 ctermfg=66 cterm=NONE guibg=#65737e guifg=#65737e gui=NONE
    hi Visual ctermbg=59 ctermfg=188 cterm=NONE guibg=#4f6166 guifg=#d9e6e9 gui=NONE
    hi VisualNOS ctermbg=66 ctermfg=188 cterm=NONE guibg=#65737e guifg=#d9e6e9 gui=NONE
    hi WildMenu ctermbg=31 ctermfg=16 cterm=NONE guibg=#2d98be guifg=#0d1a1e gui=NONE
    hi SignColumn ctermbg=188 ctermfg=179 cterm=NONE guibg=#cedbde guifg=#dbaa37 gui=NONE
    hi ErrorMsg ctermbg=188 ctermfg=131 cterm=NONE guibg=#d9e6e9 guifg=#bf4130 gui=NONE
    hi ModeMsg ctermbg=188 ctermfg=66 cterm=bold guibg=#d9e6e9 guifg=#65737e gui=bold
    hi MoreMsg ctermbg=188 ctermfg=59 cterm=bold guibg=#d9e6e9 guifg=#344246 gui=bold
    hi Question ctermbg=188 ctermfg=59 cterm=bold guibg=#d9e6e9 guifg=#344246 gui=bold
    hi WarningMsg ctermbg=188 ctermfg=131 cterm=NONE guibg=#d9e6e9 guifg=#bf4130 gui=NONE
    hi CocErrorSign ctermbg=188 ctermfg=131 cterm=NONE guibg=#cedbde guifg=#bf4130 gui=NONE
    hi CocWarningSign ctermbg=188 ctermfg=179 cterm=NONE guibg=#cedbde guifg=#dbaa37 gui=NONE
    hi CocInfoSign ctermbg=188 ctermfg=59 cterm=NONE guibg=#cedbde guifg=#344246 gui=NONE
    hi CocHintSign ctermbg=188 ctermfg=62 cterm=NONE guibg=#cedbde guifg=#3354cc gui=NONE
    hi User1 ctermbg=31 ctermfg=16 cterm=NONE guibg=#2d98be guifg=#0d1a1e gui=NONE
    hi User2 ctermbg=145 ctermfg=59 cterm=NONE guibg=#a7b6ba guifg=#344246 gui=NONE
    hi User3 ctermbg=188 ctermfg=145 cterm=NONE guibg=#cedbde guifg=#a7b6ba gui=NONE
    hi User4 ctermbg=188 ctermfg=59 cterm=NONE guibg=#cedbde guifg=#4f6166 gui=NONE
    hi User5 ctermbg=145 ctermfg=31 cterm=NONE guibg=#a7b6ba guifg=#2d98be gui=NONE
    hi Normal ctermbg=188 ctermfg=59 cterm=NONE guibg=#d9e6e9 guifg=#344246 gui=NONE

    set background=dark

    hi NonText ctermbg=188 ctermfg=62 cterm=NONE guibg=#d9e6e9 guifg=#3354cc gui=NONE
    hi SpecialKey ctermbg=NONE ctermfg=66 cterm=NONE guibg=NONE guifg=#65737e gui=NONE
    hi Comment ctermbg=NONE ctermfg=66 cterm=italic guibg=NONE guifg=#65737e gui=italic
    hi Constant ctermbg=NONE ctermfg=16 cterm=NONE guibg=NONE guifg=#0d1a1e gui=NONE
    hi String ctermbg=NONE ctermfg=31 cterm=NONE guibg=NONE guifg=#2d98be gui=NONE
    hi Special ctermbg=NONE ctermfg=59 cterm=NONE guibg=NONE guifg=#4f6166 gui=NONE
    hi Identifier ctermbg=NONE ctermfg=16 cterm=NONE guibg=NONE guifg=#0d1a1e gui=NONE
    hi PreProc ctermbg=NONE ctermfg=59 cterm=bold guibg=NONE guifg=#344246 gui=bold
    hi Statement ctermbg=NONE ctermfg=59 cterm=bold guibg=NONE guifg=#344246 gui=bold
    hi Type ctermbg=NONE ctermfg=59 cterm=bold guibg=NONE guifg=#344246 gui=bold
    hi Error ctermbg=NONE ctermfg=131 cterm=NONE guibg=NONE guifg=#bf4130 gui=NONE
    hi Todo ctermbg=188 ctermfg=215 cterm=NONE guibg=#cedbde guifg=#f9c13e gui=NONE
    hi MatchParen ctermbg=145 ctermfg=NONE cterm=NONE guibg=#a7b6ba guifg=NONE gui=NONE
    hi Underlined ctermbg=NONE ctermfg=35 cterm=underline guibg=NONE guifg=#2fbd55 gui=underline
    hi DiffAdd ctermbg=188 ctermfg=35 cterm=bold guibg=#cedbde guifg=#2fbd55 gui=bold
    hi DiffChange ctermbg=188 ctermfg=NONE cterm=NONE guibg=#cedbde guifg=NONE gui=NONE
    hi DiffDelete ctermbg=188 ctermfg=131 cterm=bold guibg=#cedbde guifg=#bf4130 gui=bold
    hi DiffText ctermbg=188 ctermfg=179 cterm=bold guibg=#cedbde guifg=#dbaa37 gui=bold
    hi IncSearch ctermbg=31 ctermfg=16 cterm=NONE guibg=#2d98be guifg=#0d1a1e gui=NONE
    hi Search ctermbg=145 ctermfg=16 cterm=NONE guibg=#a7b6ba guifg=#0d1a1e gui=NONE
    hi FoldColumn ctermbg=188 ctermfg=145 cterm=NONE guibg=#d9e6e9 guifg=#a7b6ba gui=NONE
    hi Folded ctermbg=188 ctermfg=66 cterm=NONE guibg=#cedbde guifg=#65737e gui=NONE
    hi Ignore ctermbg=NONE ctermfg=62 cterm=NONE guibg=NONE guifg=#3354cc gui=NONE
    hi Directory ctermbg=NONE ctermfg=31 cterm=NONE guibg=NONE guifg=#2d98be gui=NONE
    hi SpellBad ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#bf4130
    hi SpellCap ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#3354cc
    hi SpellLocal ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#a935d4
    hi SpellRare ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#2d98be
    hi QuickFixLine ctermbg=152 ctermfg=16 cterm=NONE guibg=#c1ccce guifg=#0d1a1e gui=NONE
    hi pythonEscape ctermbg=NONE ctermfg=130 cterm=NONE guibg=NONE guifg=#b57026 gui=NONE
    hi hsConSym ctermbg=NONE ctermfg=59 cterm=NONE guibg=NONE guifg=#4f6166 gui=NONE
    hi hsVarSym ctermbg=NONE ctermfg=59 cterm=NONE guibg=NONE guifg=#4f6166 gui=NONE
    hi ConId ctermbg=NONE ctermfg=NONE cterm=italic guibg=NONE guifg=NONE gui=italic

elseif &t_Co == 8 || $TERM !~# '^linux' || &t_Co == 16
    set t_Co=16

    hi Cursor ctermbg=darkred ctermfg=darkgrey cterm=NONE
    hi CursorLine ctermbg=white ctermfg=NONE cterm=NONE
    hi CursorColumn ctermbg=white ctermfg=NONE cterm=NONE
    hi ColorColumn ctermbg=NONE ctermfg=darkred cterm=NONE
    hi StatusLine ctermbg=white ctermfg=grey cterm=bold
    hi StatusLineNC ctermbg=white ctermfg=grey cterm=NONE
    hi StatusLineTerm ctermbg=white ctermfg=grey cterm=bold
    hi StatusLineTermNC ctermbg=white ctermfg=grey cterm=NONE
    hi LineNr ctermbg=white ctermfg=white cterm=NONE
    hi CursorLineNr ctermbg=white ctermfg=grey cterm=bold
    hi VertSplit ctermbg=white ctermfg=white cterm=NONE
    hi TabLine ctermbg=white ctermfg=grey cterm=NONE
    hi TabLineFill ctermbg=white ctermfg=grey cterm=NONE
    hi TabLineSel ctermbg=white ctermfg=grey cterm=bold
    hi Title ctermbg=NONE ctermfg=grey cterm=NONE
    hi Pmenu ctermbg=white ctermfg=grey cterm=NONE
    hi PmenuSbar ctermbg=white ctermfg=white cterm=NONE
    hi PmenuSel ctermbg=darkcyan ctermfg=black cterm=NONE
    hi PmenuThumb ctermbg=grey ctermfg=grey cterm=NONE
    hi Visual ctermbg=grey ctermfg=white cterm=NONE
    hi VisualNOS ctermbg=grey ctermfg=white cterm=NONE
    hi WildMenu ctermbg=darkcyan ctermfg=black cterm=NONE
    hi SignColumn ctermbg=white ctermfg=darkyellow cterm=NONE
    hi ErrorMsg ctermbg=white ctermfg=darkred cterm=NONE
    hi ModeMsg ctermbg=white ctermfg=grey cterm=bold
    hi MoreMsg ctermbg=white ctermfg=darkgrey cterm=bold
    hi Question ctermbg=white ctermfg=darkgrey cterm=bold
    hi WarningMsg ctermbg=white ctermfg=darkred cterm=NONE
    hi CocErrorSign ctermbg=white ctermfg=darkred cterm=NONE
    hi CocWarningSign ctermbg=white ctermfg=darkyellow cterm=NONE
    hi CocInfoSign ctermbg=white ctermfg=darkgrey cterm=NONE
    hi CocHintSign ctermbg=white ctermfg=darkblue cterm=NONE
    hi User1 ctermbg=darkcyan ctermfg=black cterm=NONE
    hi User2 ctermbg=white ctermfg=darkgrey cterm=NONE
    hi User3 ctermbg=white ctermfg=white cterm=NONE
    hi User4 ctermbg=white ctermfg=grey cterm=NONE
    hi User5 ctermbg=white ctermfg=darkcyan cterm=NONE
    hi Normal ctermbg=white ctermfg=darkgrey cterm=NONE

    set background=dark

    hi NonText ctermbg=white ctermfg=darkblue cterm=NONE
    hi SpecialKey ctermbg=NONE ctermfg=grey cterm=NONE
    hi Comment ctermbg=NONE ctermfg=grey cterm=italic
    hi Constant ctermbg=NONE ctermfg=black cterm=NONE
    hi String ctermbg=NONE ctermfg=darkcyan cterm=NONE
    hi Special ctermbg=NONE ctermfg=grey cterm=NONE
    hi Identifier ctermbg=NONE ctermfg=black cterm=NONE
    hi PreProc ctermbg=NONE ctermfg=darkgrey cterm=bold
    hi Statement ctermbg=NONE ctermfg=darkgrey cterm=bold
    hi Type ctermbg=NONE ctermfg=darkgrey cterm=bold
    hi Error ctermbg=NONE ctermfg=darkred cterm=NONE
    hi Todo ctermbg=white ctermfg=yellow cterm=NONE
    hi MatchParen ctermbg=white ctermfg=NONE cterm=NONE
    hi Underlined ctermbg=NONE ctermfg=darkgreen cterm=underline
    hi DiffAdd ctermbg=white ctermfg=darkgreen cterm=bold
    hi DiffChange ctermbg=white ctermfg=NONE cterm=NONE
    hi DiffDelete ctermbg=white ctermfg=darkred cterm=bold
    hi DiffText ctermbg=white ctermfg=darkyellow cterm=bold
    hi IncSearch ctermbg=darkcyan ctermfg=black cterm=NONE
    hi Search ctermbg=white ctermfg=black cterm=NONE
    hi FoldColumn ctermbg=white ctermfg=white cterm=NONE
    hi Folded ctermbg=white ctermfg=grey cterm=NONE
    hi Ignore ctermbg=NONE ctermfg=darkblue cterm=NONE
    hi Directory ctermbg=NONE ctermfg=darkcyan cterm=NONE
    hi SpellBad ctermbg=NONE ctermfg=NONE cterm=underline
    hi SpellCap ctermbg=NONE ctermfg=NONE cterm=underline
    hi SpellLocal ctermbg=NONE ctermfg=NONE cterm=underline
    hi SpellRare ctermbg=NONE ctermfg=NONE cterm=underline
    hi QuickFixLine ctermbg=white ctermfg=black cterm=NONE
    hi pythonEscape ctermbg=NONE ctermfg=darkred cterm=NONE
    hi hsConSym ctermbg=NONE ctermfg=grey cterm=NONE
    hi hsVarSym ctermbg=NONE ctermfg=grey cterm=NONE
    hi ConId ctermbg=NONE ctermfg=NONE cterm=italic
endif

hi link Number Constant
hi link pythonDocstring Comment
hi link pythonAs Statement
hi link markdownCodeDelimiter String
hi link markdownCode String
hi link markdownHeadingDelimiter Statement
hi link markdownH1 Statement
hi link markdownH2 Statement
hi link markdownH3 Statement
hi link markdownH4 Statement
hi link markdownH5 Statement
hi link markdownH6 Statement
hi link markdownCodeDelimiter String
hi link markdownCode String
hi link markdownHeadingDelimiter Statement
hi link markdownH1 Statement
hi link markdownH2 Statement
hi link markdownH3 Statement
hi link markdownH4 Statement
hi link markdownH5 Statement
hi link markdownH6 Statement
hi link markdownLinkText Underlined
hi link markdownLinkTextDelimiter Underlined
hi link markdownLinkDelimiter Underlined
hi link markdownUrl Underlined
hi link VimwikiHeaderChar Statement
hi link VimwikiHeader1 Statement
hi link VimwikiHeader2 Statement
hi link VimwikiHeader3 Statement
hi link VimwikiHeader4 Statement
hi link VimwikiHeader5 Statement
hi link VimwikiHeader6 Statement
hi link VimwikiCodeChar Sting
hi link VimwikiCode String
hi link VimwikiPre String
hi link VimwikiListTodo Statement
hi link hsCharacter String

let g:terminal_ansi_colors = [
        \ '#d9e6e9',
        \ '#bf4130',
        \ '#2fbd55',
        \ '#dbaa37',
        \ '#3354cc',
        \ '#a935d4',
        \ '#2d98be',
        \ '#344246',
        \ '#c1ccce',
        \ '#dd4837',
        \ '#35db5f',
        \ '#f9c13e',
        \ '#3a61ea',
        \ '#bf3cf2',
        \ '#34b2d1',
        \ '#0d1a1e',
        \ ]

" Generated with RNB (https://gist.github.com/romainl/5cd2f4ec222805f49eca)
