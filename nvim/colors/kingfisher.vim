" kingfisher.vim -- Vim color scheme.
" Author:      Fabian Neuschmidt (fabian@neuschmidt.de)
" Webpage:     http://www.example.com
" Description: A minimal teal colorscheme for (neo)vim

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "kingfisher"

if ($TERM =~ '256' || &t_Co >= 256) || has("gui_running")
    hi Cursor ctermbg=131 ctermfg=152 cterm=NONE guibg=#bf4130 guifg=#c1ccce gui=NONE
    hi CursorLine ctermbg=17 ctermfg=NONE cterm=NONE guibg=#1b2f34 guifg=NONE gui=NONE
    hi CursorColumn ctermbg=17 ctermfg=NONE cterm=NONE guibg=#1b2f34 guifg=NONE gui=NONE
    hi ColorColumn ctermbg=NONE ctermfg=131 cterm=NONE guibg=NONE guifg=#bf4130 gui=NONE
    hi StatusLine ctermbg=17 ctermfg=145 cterm=bold guibg=#1b2f34 guifg=#a7b6ba gui=bold
    hi StatusLineNC ctermbg=17 ctermfg=66 cterm=NONE guibg=#1b2f34 guifg=#65737e gui=NONE
    hi StatusLineTerm ctermbg=17 ctermfg=145 cterm=bold guibg=#1b2f34 guifg=#a7b6ba gui=bold
    hi StatusLineTermNC ctermbg=17 ctermfg=66 cterm=NONE guibg=#1b2f34 guifg=#65737e gui=NONE
    hi LineNr ctermbg=16 ctermfg=59 cterm=NONE guibg=#0d1a1e guifg=#4f6166 gui=NONE
    hi CursorLineNr ctermbg=17 ctermfg=66 cterm=bold guibg=#1b2f34 guifg=#65737e gui=bold
    hi VertSplit ctermbg=16 ctermfg=59 cterm=NONE guibg=#0d1a1e guifg=#4f6166 gui=NONE
    hi TabLine ctermbg=17 ctermfg=66 cterm=NONE guibg=#1b2f34 guifg=#65737e gui=NONE
    hi TabLineFill ctermbg=17 ctermfg=66 cterm=NONE guibg=#1b2f34 guifg=#65737e gui=NONE
    hi TabLineSel ctermbg=17 ctermfg=145 cterm=bold guibg=#1b2f34 guifg=#a7b6ba gui=bold
    hi Title ctermbg=NONE ctermfg=66 cterm=NONE guibg=NONE guifg=#65737e gui=NONE
    hi Pmenu ctermbg=17 ctermfg=145 cterm=NONE guibg=#1b2f34 guifg=#a7b6ba gui=NONE
    hi PmenuSbar ctermbg=59 ctermfg=59 cterm=NONE guibg=#344246 guifg=#344246 gui=NONE
    hi PmenuSel ctermbg=31 ctermfg=188 cterm=NONE guibg=#2d98be guifg=#d9e6e9 gui=NONE
    hi PmenuThumb ctermbg=66 ctermfg=66 cterm=NONE guibg=#65737e guifg=#65737e gui=NONE
    hi Visual ctermbg=145 ctermfg=16 cterm=NONE guibg=#a7b6ba guifg=#0d1a1e gui=NONE
    hi VisualNOS ctermbg=66 ctermfg=16 cterm=NONE guibg=#65737e guifg=#0d1a1e gui=NONE
    hi WildMenu ctermbg=31 ctermfg=188 cterm=NONE guibg=#2d98be guifg=#d9e6e9 gui=NONE
    hi SignColumn ctermbg=17 ctermfg=179 cterm=NONE guibg=#1b2f34 guifg=#dbaa37 gui=NONE
    hi ErrorMsg ctermbg=16 ctermfg=131 cterm=NONE guibg=#0d1a1e guifg=#bf4130 gui=NONE
    hi ModeMsg ctermbg=16 ctermfg=66 cterm=bold guibg=#0d1a1e guifg=#65737e gui=bold
    hi MoreMsg ctermbg=16 ctermfg=152 cterm=bold guibg=#0d1a1e guifg=#c1ccce gui=bold
    hi Question ctermbg=16 ctermfg=152 cterm=bold guibg=#0d1a1e guifg=#c1ccce gui=bold
    hi WarningMsg ctermbg=16 ctermfg=131 cterm=NONE guibg=#0d1a1e guifg=#bf4130 gui=NONE
    hi CocErrorSign ctermbg=17 ctermfg=131 cterm=NONE guibg=#1b2f34 guifg=#bf4130 gui=NONE
    hi CocWarningSign ctermbg=17 ctermfg=179 cterm=NONE guibg=#1b2f34 guifg=#dbaa37 gui=NONE
    hi CocInfoSign ctermbg=17 ctermfg=152 cterm=NONE guibg=#1b2f34 guifg=#c1ccce gui=NONE
    hi CocHintSign ctermbg=17 ctermfg=62 cterm=NONE guibg=#1b2f34 guifg=#3354cc gui=NONE
    hi User1 ctermbg=31 ctermfg=188 cterm=NONE guibg=#2d98be guifg=#d9e6e9 gui=NONE
    hi User2 ctermbg=59 ctermfg=152 cterm=NONE guibg=#4f6166 guifg=#c1ccce gui=NONE
    hi User3 ctermbg=17 ctermfg=59 cterm=NONE guibg=#1b2f34 guifg=#4f6166 gui=NONE
    hi User4 ctermbg=17 ctermfg=145 cterm=NONE guibg=#1b2f34 guifg=#a7b6ba gui=NONE
    hi User5 ctermbg=59 ctermfg=31 cterm=NONE guibg=#4f6166 guifg=#2d98be gui=NONE
    hi Normal ctermbg=16 ctermfg=152 cterm=NONE guibg=#0d1a1e guifg=#c1ccce gui=NONE

    set background=dark

    hi NonText ctermbg=16 ctermfg=62 cterm=NONE guibg=#0d1a1e guifg=#3354cc gui=NONE
    hi SpecialKey ctermbg=NONE ctermfg=66 cterm=NONE guibg=NONE guifg=#65737e gui=NONE
    hi Comment ctermbg=NONE ctermfg=66 cterm=italic guibg=NONE guifg=#65737e gui=italic
    hi Constant ctermbg=NONE ctermfg=188 cterm=NONE guibg=NONE guifg=#d9e6e9 gui=NONE
    hi String ctermbg=NONE ctermfg=31 cterm=NONE guibg=NONE guifg=#2d98be gui=NONE
    hi Special ctermbg=NONE ctermfg=145 cterm=NONE guibg=NONE guifg=#a7b6ba gui=NONE
    hi Identifier ctermbg=NONE ctermfg=188 cterm=NONE guibg=NONE guifg=#d9e6e9 gui=NONE
    hi PreProc ctermbg=NONE ctermfg=152 cterm=bold guibg=NONE guifg=#c1ccce gui=bold
    hi Statement ctermbg=NONE ctermfg=152 cterm=bold guibg=NONE guifg=#c1ccce gui=bold
    hi Type ctermbg=NONE ctermfg=152 cterm=bold guibg=NONE guifg=#c1ccce gui=bold
    hi Error ctermbg=NONE ctermfg=131 cterm=NONE guibg=NONE guifg=#bf4130 gui=NONE
    hi Todo ctermbg=17 ctermfg=215 cterm=NONE guibg=#1b2f34 guifg=#f9c13e gui=NONE
    hi MatchParen ctermbg=59 ctermfg=NONE cterm=NONE guibg=#4f6166 guifg=NONE gui=NONE
    hi Underlined ctermbg=NONE ctermfg=35 cterm=underline guibg=NONE guifg=#2fbd55 gui=underline
    hi DiffAdd ctermbg=17 ctermfg=35 cterm=bold guibg=#1b2f34 guifg=#2fbd55 gui=bold
    hi DiffChange ctermbg=17 ctermfg=NONE cterm=NONE guibg=#1b2f34 guifg=NONE gui=NONE
    hi DiffDelete ctermbg=17 ctermfg=131 cterm=bold guibg=#1b2f34 guifg=#bf4130 gui=bold
    hi DiffText ctermbg=17 ctermfg=179 cterm=bold guibg=#1b2f34 guifg=#dbaa37 gui=bold
    hi IncSearch ctermbg=31 ctermfg=188 cterm=NONE guibg=#2d98be guifg=#d9e6e9 gui=NONE
    hi Search ctermbg=59 ctermfg=188 cterm=NONE guibg=#4f6166 guifg=#d9e6e9 gui=NONE
    hi FoldColumn ctermbg=16 ctermfg=59 cterm=NONE guibg=#0d1a1e guifg=#4f6166 gui=NONE
    hi Folded ctermbg=17 ctermfg=66 cterm=NONE guibg=#1b2f34 guifg=#65737e gui=NONE
    hi Ignore ctermbg=NONE ctermfg=62 cterm=NONE guibg=NONE guifg=#3354cc gui=NONE
    hi Directory ctermbg=NONE ctermfg=31 cterm=NONE guibg=NONE guifg=#2d98be gui=NONE
    hi SpellBad ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#bf4130
    hi SpellCap ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#3354cc
    hi SpellLocal ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#a935d4
    hi SpellRare ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline guisp=#2d98be
    hi QuickFixLine ctermbg=59 ctermfg=188 cterm=NONE guibg=#344246 guifg=#d9e6e9 gui=NONE
    hi pythonEscape ctermbg=NONE ctermfg=130 cterm=NONE guibg=NONE guifg=#b57026 gui=NONE
    hi hsConSym ctermbg=NONE ctermfg=145 cterm=NONE guibg=NONE guifg=#a7b6ba gui=NONE
    hi hsVarSym ctermbg=NONE ctermfg=145 cterm=NONE guibg=NONE guifg=#a7b6ba gui=NONE
    hi ConId ctermbg=NONE ctermfg=NONE cterm=italic guibg=NONE guifg=NONE gui=italic

elseif &t_Co == 8 || $TERM !~# '^linux' || &t_Co == 16
    set t_Co=16

    hi Cursor ctermbg=darkred ctermfg=white cterm=NONE
    hi CursorLine ctermbg=darkgrey ctermfg=NONE cterm=NONE
    hi CursorColumn ctermbg=darkgrey ctermfg=NONE cterm=NONE
    hi ColorColumn ctermbg=NONE ctermfg=darkred cterm=NONE
    hi StatusLine ctermbg=darkgrey ctermfg=white cterm=bold
    hi StatusLineNC ctermbg=darkgrey ctermfg=grey cterm=NONE
    hi StatusLineTerm ctermbg=darkgrey ctermfg=white cterm=bold
    hi StatusLineTermNC ctermbg=darkgrey ctermfg=grey cterm=NONE
    hi LineNr ctermbg=black ctermfg=grey cterm=NONE
    hi CursorLineNr ctermbg=darkgrey ctermfg=grey cterm=bold
    hi VertSplit ctermbg=black ctermfg=grey cterm=NONE
    hi TabLine ctermbg=darkgrey ctermfg=grey cterm=NONE
    hi TabLineFill ctermbg=darkgrey ctermfg=grey cterm=NONE
    hi TabLineSel ctermbg=darkgrey ctermfg=white cterm=bold
    hi Title ctermbg=NONE ctermfg=grey cterm=NONE
    hi Pmenu ctermbg=darkgrey ctermfg=white cterm=NONE
    hi PmenuSbar ctermbg=darkgrey ctermfg=darkgrey cterm=NONE
    hi PmenuSel ctermbg=darkcyan ctermfg=white cterm=NONE
    hi PmenuThumb ctermbg=grey ctermfg=grey cterm=NONE
    hi Visual ctermbg=white ctermfg=black cterm=NONE
    hi VisualNOS ctermbg=grey ctermfg=black cterm=NONE
    hi WildMenu ctermbg=darkcyan ctermfg=white cterm=NONE
    hi SignColumn ctermbg=darkgrey ctermfg=darkyellow cterm=NONE
    hi ErrorMsg ctermbg=black ctermfg=darkred cterm=NONE
    hi ModeMsg ctermbg=black ctermfg=grey cterm=bold
    hi MoreMsg ctermbg=black ctermfg=white cterm=bold
    hi Question ctermbg=black ctermfg=white cterm=bold
    hi WarningMsg ctermbg=black ctermfg=darkred cterm=NONE
    hi CocErrorSign ctermbg=darkgrey ctermfg=darkred cterm=NONE
    hi CocWarningSign ctermbg=darkgrey ctermfg=darkyellow cterm=NONE
    hi CocInfoSign ctermbg=darkgrey ctermfg=white cterm=NONE
    hi CocHintSign ctermbg=darkgrey ctermfg=darkblue cterm=NONE
    hi User1 ctermbg=darkcyan ctermfg=white cterm=NONE
    hi User2 ctermbg=grey ctermfg=white cterm=NONE
    hi User3 ctermbg=darkgrey ctermfg=grey cterm=NONE
    hi User4 ctermbg=darkgrey ctermfg=white cterm=NONE
    hi User5 ctermbg=grey ctermfg=darkcyan cterm=NONE
    hi Normal ctermbg=black ctermfg=white cterm=NONE

    set background=dark

    hi NonText ctermbg=black ctermfg=darkblue cterm=NONE
    hi SpecialKey ctermbg=NONE ctermfg=grey cterm=NONE
    hi Comment ctermbg=NONE ctermfg=grey cterm=italic
    hi Constant ctermbg=NONE ctermfg=white cterm=NONE
    hi String ctermbg=NONE ctermfg=darkcyan cterm=NONE
    hi Special ctermbg=NONE ctermfg=white cterm=NONE
    hi Identifier ctermbg=NONE ctermfg=white cterm=NONE
    hi PreProc ctermbg=NONE ctermfg=white cterm=bold
    hi Statement ctermbg=NONE ctermfg=white cterm=bold
    hi Type ctermbg=NONE ctermfg=white cterm=bold
    hi Error ctermbg=NONE ctermfg=darkred cterm=NONE
    hi Todo ctermbg=darkgrey ctermfg=yellow cterm=NONE
    hi MatchParen ctermbg=grey ctermfg=NONE cterm=NONE
    hi Underlined ctermbg=NONE ctermfg=darkgreen cterm=underline
    hi DiffAdd ctermbg=darkgrey ctermfg=darkgreen cterm=bold
    hi DiffChange ctermbg=darkgrey ctermfg=NONE cterm=NONE
    hi DiffDelete ctermbg=darkgrey ctermfg=darkred cterm=bold
    hi DiffText ctermbg=darkgrey ctermfg=darkyellow cterm=bold
    hi IncSearch ctermbg=darkcyan ctermfg=white cterm=NONE
    hi Search ctermbg=grey ctermfg=white cterm=NONE
    hi FoldColumn ctermbg=black ctermfg=grey cterm=NONE
    hi Folded ctermbg=darkgrey ctermfg=grey cterm=NONE
    hi Ignore ctermbg=NONE ctermfg=darkblue cterm=NONE
    hi Directory ctermbg=NONE ctermfg=darkcyan cterm=NONE
    hi SpellBad ctermbg=NONE ctermfg=NONE cterm=underline
    hi SpellCap ctermbg=NONE ctermfg=NONE cterm=underline
    hi SpellLocal ctermbg=NONE ctermfg=NONE cterm=underline
    hi SpellRare ctermbg=NONE ctermfg=NONE cterm=underline
    hi QuickFixLine ctermbg=darkgrey ctermfg=white cterm=NONE
    hi pythonEscape ctermbg=NONE ctermfg=darkred cterm=NONE
    hi hsConSym ctermbg=NONE ctermfg=white cterm=NONE
    hi hsVarSym ctermbg=NONE ctermfg=white cterm=NONE
    hi ConId ctermbg=NONE ctermfg=NONE cterm=italic
endif

hi link Number Constant
hi link pythonDocstring Comment
hi link pythonAs Statement
hi link markdownCodeDelimiter String
hi link markdownCode String
hi link markdownHeadingDelimiter Statement
hi link markdownH1 Statement
hi link markdownH2 Statement
hi link markdownH3 Statement
hi link markdownH4 Statement
hi link markdownH5 Statement
hi link markdownH6 Statement
hi link markdownCodeDelimiter String
hi link markdownCode String
hi link markdownHeadingDelimiter Statement
hi link markdownH1 Statement
hi link markdownH2 Statement
hi link markdownH3 Statement
hi link markdownH4 Statement
hi link markdownH5 Statement
hi link markdownH6 Statement
hi link markdownLinkText Underlined
hi link markdownLinkTextDelimiter Underlined
hi link markdownLinkDelimiter Underlined
hi link markdownUrl Underlined
hi link VimwikiHeaderChar Statement
hi link VimwikiHeader1 Statement
hi link VimwikiHeader2 Statement
hi link VimwikiHeader3 Statement
hi link VimwikiHeader4 Statement
hi link VimwikiHeader5 Statement
hi link VimwikiHeader6 Statement
hi link VimwikiCodeChar Sting
hi link VimwikiCode String
hi link VimwikiPre String
hi link VimwikiListTodo Statement
hi link hsCharacter String

let g:terminal_ansi_colors = [
        \ '#0d1a1e',
        \ '#bf4130',
        \ '#2fbd55',
        \ '#dbaa37',
        \ '#3354cc',
        \ '#a935d4',
        \ '#2d98be',
        \ '#c1ccce',
        \ '#344246',
        \ '#dd4837',
        \ '#35db5f',
        \ '#f9c13e',
        \ '#3a61ea',
        \ '#bf3cf2',
        \ '#34b2d1',
        \ '#d9e6e9',
        \ ]

" Generated with RNB (https://gist.github.com/romainl/5cd2f4ec222805f49eca)
