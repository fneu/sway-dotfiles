" Highlight docstrings as Comments, not strings
syn region pythonDocstring start=+^\s*[uU]\?[rR]\?"""+ end=+"""+ keepend
  \ excludenl contains=pythonEscape,@Spell,pythonSpaceError
syn region pythonDocstring start=+^\s*[uU]\?[rR]\?'''+ end=+'''+ keepend
  \ excludenl contains=pythonEscape,@Spell,pythonSpaceError
" Make 'as' bold, same as 'import' and 'from'
syn keyword pythonAs as
