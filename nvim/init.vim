
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
    silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif

call plug#begin()
" Theme
Plug 'fneu/breezy'

" LSP, completion, linting
" Install the following extensions after coc is installed:
"   `:CocInstall <extension>`
" coc keeps them up to date
"   coc-pairs
"   coc-json
"   coc-git
"   coc-lists
"   coc-snippets  - <Enter> to accept, <C-j> & <C-k> to jump
"   coc-python
"   coc-texlab?
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" snippets
Plug 'honza/vim-snippets'

" fuzzy file search
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --bin'}
Plug 'junegunn/fzf.vim'

" tpope
Plug 'tpope/vim-fugitive'    " git integration
Plug 'tpope/vim-commentary'  " comment with gcc / gc<motion>
Plug 'tpope/vim-surround'    " cs, ds, ys(s), v_S surroundings
Plug 'tpope/vim-repeat'      " repeat surrounds repeatable
Plug 'tpope/vim-vinegar'     " netrw improvements, spawn with '-'
Plug 'tpope/vim-unimpaired'  " various ]/[-bindings
Plug 'tpope/vim-eunuch'      " :SudoWrite, :Rename, ...

" wiki
Plug 'vimwiki/vimwiki'
call plug#end()


" VISUALS

set termguicolors
set background=light
colorscheme breezy
hi User1 ctermbg=31 ctermfg=188 cterm=NONE guibg=#1b668f guifg=#fcfcfc gui=NONE
hi User2 ctermbg=59 ctermfg=152 cterm=NONE guibg=#63686d guifg=#cdd3d7 gui=NONE
hi User3 ctermbg=17 ctermfg=59 cterm=NONE guibg=#31363b guifg=#63686d gui=NONE
hi User4 ctermbg=17 ctermfg=145 cterm=NONE guibg=#31363b guifg=#bdc3c7 gui=NONE
hi User5 ctermbg=59 ctermfg=31 cterm=NONE guibg=#63686d guifg=#1b668f gui=NONE


" BASIC SETUP

" some of neovims defaults differ from vim:
" https://neovim.io/doc/user/vim_diff.html
set tabstop=8                       " size of actual tab chars
set softtabstop=4                   " 'width' of a tab inserting operation
set shiftwidth=4                    " 'width' of an indenting operation
set expandtab                       " indent using spaces instead of tabs
set hidden                          " if not set, coc TextEdit might fail
set ignorecase                      " case insensitive search ...
set smartcase                       " ... unless pattern contains capitals
set shortmess+=c                    " suppress ins-complete-menu messages
set cmdheight=1                     " Better display for messages, mostly coc
set signcolumn=yes                  " always show signcolumn
set noshowmode                      " suppress mode messages
set wildmenu                        " command line completion
set wildignore+=*.o,*.obj,.git,*.rbc,*.pyc,__pycache__
set ruler                           " show current position in statusline
set scrolloff=3                     " always show n lines above/below cursor
set sidescrolloff=5                 " ... same thing for sidescroll
set showbreak=↪\ "
set listchars=tab:→\ ,nbsp:␣,trail:·,extends:›,precedes:‹
set list                            " highlight above chars respectively
set modeline                        " read settings from modelines opened files
set undofile                        " keep undo history (forever)
set mouse=a                         " use mouse in all modes
set updatetime=300                  " faster CursorHold & CursorHoldI
set nobackup                        " Some LSPs have problems with backup files
set nowritebackup                   " ↑ same thing, check coc #649
set foldmethod=marker               " manual folds with {{{ and }}}

" " restore cursor position when opening file
" function! ResCur()
"   if line("'\"") <= line("$")
"     normal! g`"
"     return 1
"   endif
" endfunction

" augroup resCur
"   autocmd!
"   autocmd BufWinEnter * call ResCur()
" augroup END

" MAPPINGS

let mapleader = " "
let maplocalleader = " "

" follow tags, help, etc.
nnoremap <leader>t <C-]>

" center after search
nnoremap n nzz
nnoremap N Nzz

" use <C-l> to clear the highlighting of :hlsearch
nnoremap <silent> <C-l>
\ :nohlsearch<C-r>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-l>

" compensate for German keyboard layout
" can be used in vim-unimpaired bindings
nmap ö [
nmap ä ]
omap ö [
omap ä ]
xmap ö [
xmap ä ]

" git
nnoremap <Leader>g :Gstatus<CR>

" search
nnoremap <silent> <leader>f :Files<CR>
nnoremap <silent> <leader><S-f> :Files ~<CR>
nnoremap <silent> <leader>b :Buffers<CR>
nnoremap <silent> <leader>p :Rg<CR>

" show highlight group used at the cursor
map <leader>h :echo "hi<" . synIDattr(synID(line("."),col("."),1),"name") . '> trans<'
\ . synIDattr(synID(line("."),col("."),0),"name") . "> lo<"
\ . synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"<CR>

" PLUGIN SETTINGS

" coc.nvim

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)

" coc-git navigation

" go to git chunks
nmap [g <Plug>(coc-git-prevchunk)
nmap ]g <Plug>(coc-git-nextchunk)

" create text object for git chunks
omap ig <Plug>(coc-git-chunk-inner)
xmap ig <Plug>(coc-git-chunk-inner)
omap ag <Plug>(coc-git-chunk-outer)
xmap ag <Plug>(coc-git-chunk-outer)


" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap for rename current word
nmap <leader>r <Plug>(coc-rename)

" Remap for format selected region
xmap <leader>=  <Plug>(coc-format-selected)
nmap <leader>=  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" Use <tab> for select selections ranges, needs server support, like: coc-tsserver, coc-python
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <S-TAB> <Plug>(coc-range-select-backword)

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>ca  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>ce  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>cc  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>co  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>cs  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>cj  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>ck  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>cp  :<C-u>CocListResume<CR>

" fzf

" open fzf in current window
let g:fzf_layout = { 'window': 'enew' }

" quit fzf with <esc>
augroup fzf
    autocmd!
    autocmd FileType fzf tnoremap <nowait><buffer> <esc> <c-g>
augroup END

" ignore some potentially large folders
let $FZF_DEFAULT_COMMAND =  "find * -path '*/\.*' -prune -o -path 'node_modules/**' -prune -o -path 'target/**' -prune -o -path 'dist/**' -prune -o  -type f -print -o -type l -print 2> /dev/null"

" use ripgrep if available
if executable('rg')
  let $FZF_DEFAULT_COMMAND = 'rg --files --hidden --follow --glob "!.git/*"'
  set grepprg=rg\ --vimgrep
endif

" vimwiki

let g:vimwiki_list = [{'path': '~/vimwiki/wiki/',
  \ 'path_html': '~/vimwiki/html/',
  \ 'syntax': 'markdown',
  \ 'ext': '.md',
  \ 'custom_wiki2html': $HOME.'/vimwiki/wiki2html.sh'}]

let g:vimwiki_ext2syntax = {'.md': 'markdown'}
let g:vimwiki_global_ext = 0

" LANGUAGE SPECIFIC SETTINGS

augroup languages
    autocmd!
    autocmd FileType make setlocal noexpandtab
    autocmd Filetype html setlocal sts=2 sw=2 expandtab
    autocmd Filetype yaml setlocal sts=2 sw=2 expandtab
    autocmd FileType python setlocal colorcolumn=79
    autocmd FileType json setlocal sts=2 sw=2 expandtab
    autocmd FileType typescript setlocal sts=2 sw=2
    autocmd FileType css setlocal sts=2 sw=2
    " coc-config
    autocmd FileType json syntax match Comment +\/\/.\+$+
    " coc-pairs
    autocmd FileType tex let b:coc_pairs = [["$", "$"]]
    autocmd FileType vim let b:coc_pairs_disabled = ['"']
augroup END

" STATUSLINE

function! ActiveStatus()
  let statusline=""
  let statusline.="%1*"
  let statusline.="%(%{'help'!=&filetype?'\ \ '.bufnr('%'):''}\ %)"
  let statusline.="%5*"
  let statusline.=""
  let statusline.="%2*"
  let statusline.="%{fugitive#head()!=''?'\ \ '.fugitive#head().'\ ':''}"
  let statusline.="%3*"
  let statusline.=""
  let statusline.="%4*"
  let statusline.="\ %<"
  let statusline.="%f"
  let statusline.="%{&modified?'\ \ +':''}"
  let statusline.="%{&readonly?'\ \ ':''}"
  let statusline.="%="
  let statusline.="\ %{''!=#&filetype?&filetype:'none'}"
  let statusline.="%(\ %{(&bomb\|\|'^$\|utf-8'!~#&fileencoding?'\ '.&fileencoding.(&bomb?'-bom':''):'').('unix'!=#&fileformat?'\ '.&fileformat:'')}%)"
  "let statusline.="%(\ \ %{&modifiable?(&expandtab?'et\ ':'noet\ ').&shiftwidth:''}%)"
  let statusline.="%3*"
  let statusline.="\ "
  let statusline.="%2*"
  if exists('g:did_coc_loaded')
      let statusline.="%{coc#status()!=''?'\ '.coc#status().'\ ':''}"
  endif
  let statusline.="%5*"
  let statusline.=""
  let statusline.="%1*"
  let statusline.="\ %2v"
  let statusline.="\ %3p%%\ "
  return statusline
endfunction

function! InactiveStatus()
  let statusline=""
  let statusline.="%(%{'help'!=&filetype?'\ \ '.bufnr('%').'\ \ ':'\ '}%)"
  let statusline.="%{fugitive#head()!=''?'\ \ '.fugitive#head().'\ ':'\ '}"
  let statusline.="\ %<"
  let statusline.="%f"
  let statusline.="%{&modified?'\ \ +':''}"
  let statusline.="%{&readonly?'\ \ ':''}"
  let statusline.="%="
  let statusline.="\ %{''!=#&filetype?&filetype:'none'}"
  let statusline.="%(\ %{(&bomb\|\|'^$\|utf-8'!~#&fileencoding?'\ '.&fileencoding.(&bomb?'-bom':''):'').('unix'!=#&fileformat?'\ '.&fileformat:'')}%)"
  "let statusline.="%(\ \ %{&modifiable?(&expandtab?'et\ ':'noet\ ').&shiftwidth:''}%)"
  let statusline.="\ \ "
  let statusline.="\ %2v"
  let statusline.="\ %3p%%\ "
  return statusline
endfunction

set laststatus=2
set statusline=%!ActiveStatus()

augroup status
  autocmd!
  autocmd WinEnter * setlocal statusline=%!ActiveStatus()
  autocmd WinLeave * setlocal statusline=%!InactiveStatus()
augroup END
