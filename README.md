# dotfiles

To install on a fresh openSUSE tumbleweed system, copy ssh keys an run:

```sh
# fix ssh permissions on new machine
chmod 700 ~/.ssh
chmod 644 ~/.ssh/id_rsa.pub
chmod 600 ~/.ssh/id_rsa

# install git and friends
sudo zypper in -t pattern devel_basis

# clone repository to expected location and install
mkdir ~/.devel
git clone git@gitlab.com:fneu/dotfiles.git ~/devel/dotfiles
./devel/dotfiles/install
```

Also consider the following tweaks

- run `visudo` to configure sudo
- on encrypted systems, prevent the second password request:
    - [opensuse wiki](https://de.opensuse.org/SDB:Verschlüsseltes_root_file_system)
- on encrypted systems, enable auto-login on tty1:
    - [arch wiki](https://wiki.archlinux.org/index.php/Getty)
    - on openSUSE, use `/usr/sbin/agetty` instead of `/usr/bin/agetty`
- mount a slow shared data partition with `UUID=<uuid> /mnt/Daten ntfs defaults,auto,_netdev 0 0`

Run `cat packages/* | xargs sudo zypper install` to install all the things!

Run the scripts in `postinstall` to set up various things

install from external sources:
- [skype](https://www.skype.com/de/get-skype/download-skype-for-desktop)
- [zoom](https://zoom.us/download)
- [brother printer driver](https://support.brother.com/g/b/downloadtop.aspx?c=de&lang=de&prod=mfcl3750cdw_us_eu_as)
