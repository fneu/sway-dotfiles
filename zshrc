# BASIC ZSH SETTINGS {{{

HISTFILE=~/.histfile
HISTSIZE=1000000
SAVEHIST=1000000
# set options:
#   appendhistory: do not replace histhile, thus keeping history of all shells
#   autocd:        if the command is a directory name, go there...
#   extendedglob:  more globbing wildcards
#   menucomplete:  always complete an entire option for ambiguous completions
#   promptsubst:   allow function substitution in prompt
setopt appendhistory autocd extendedglob menucomplete promptsubst
unsetopt beep
# Show menu for ambiguous completion with more than n options
zstyle ':completion:*' menu select=4

autoload -Uz compinit promptinit colors
compinit
promptinit
colors

# make home and end work
# check alternative codes by pressing e.g. <C-v><Home> in the terminal
bindkey $'^[[H' beginning-of-line
bindkey $'^[[F' end-of-line

# up and down arrows also search
bindkey $'^[[A' up-line-or-search    # up arrow
bindkey $'^[[B' down-line-or-search  # down arrow

# ctrl-backspace deletes word
bindkey "^H" backward-delete-word

# ctrl+arrow moves by words
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

# }}}

# ALIASES AND FUNCTIONS {{{

alias e='nvim'
alias r='ranger'
alias g='git'
alias ga='git add'
alias gs='git status'
alias gc='git commit'
alias gd='git diff'
alias zi='sudo zypper install'
alias zs='zypper search'
alias pi='pip install'
alias ps='pip search'
alias pir='pip install -r requirements.txt'

# navigate dir history
alias d='dirs -v | head -10'
alias 1='cd -'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'

# directories
alias df='cd ~/devel/dotfiles'
alias viper='cd ~/devel/viperdev'
alias sem='cd ~/Dokumente/Studium/Master/Sem3'

# simplified python virtual env mangling
alias lsvenv='ls ~/.venv'
alias unvenv='deactivate'
mkvenv() {
	mkdir -p ~/.venv
	python3 -m venv ~/.venv/"$1"
}
rmvenv() {
	rm -rf ~/.venv/"$1"
}
venv() {
	source ~/.venv/"$1"/bin/activate
}

# mkcd: make directory and go there
mkcd() {
    mkdir "$@" && cd "$@"
}

# alert: Show a desktop notification when a command finishes.
# Use like this:
#   sleep 5; alert
function alert() {
    if [ $? = 0 ];
    then icon=/usr/share/icons/Papirus/32x32/emblems/vcs-normal.svg;
    else icon=/usr/share/icons/Papirus/32x32/emblems/vcs-conflicting.svg;
    fi
    last_cmd="$(history | tail -n1 | sed 's/^\s*[0-9]*\s*//' | sed 's/;\s*alert\s*$//')"
    notify-send -i $icon "$last_cmd"
}

# docker purge: add ppurge command
docker() {
    if [[ $@ == "purge" ]]; then
        command docker rm $(docker ps -a -q) && docker rmi $(docker images -q)
    else
        command docker "$@"
    fi
}

# ranger: automatically cd to the last directory opened in ranger
function ranger() {
    tempfile="$(mktemp)"
    /usr/bin/ranger --choosedir="$tempfile" "${@:-$(pwd)}"
    test -f "$tempfile" && cd -- "$(cat "$tempfile")"
    rm -f -- "$tempfile"
}

# }}}

# ENVS AND SETTINGS {{{

# ls colors
eval "$(dircolors -b ~/.dircolors)"
alias ls='ls --color=auto --group-directories-first'

#less
export LESS='-MRi#8j.5'
#             |||| `- center on search matches
#             |||`--- scroll horizontally 8 columns at a time
#             ||`---- case-insensitive search unless pattern contains uppercase
#             |`----- parse color codes
#             `------ show more information in promp

# grep
alias grep='grep --color --binary-files=without-match --exclude-dir .git'

# fzf colors
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
 --color=fg:#fbfbfb,bg:#232627,hl:#3daee9
 --color=fg+:#fefefe,bg+:#2d5c76,hl+:#11d016
 --color=info:#63686D,prompt:#63686D,pointer:#fefefe
 --color=marker:#f57400,spinner:#63686D,header:#63686D'

# }}}

# NATIVE WAYLAND {{{
# https://github.com/swaywm/sway/wiki/Running-programs-natively-under-Wayland

# GTK apps
#export GDK_BACKEND=wayland # this breaks discord and some apps that don't like wayland
export CLUTTER_BACKEND=wayland
# firefox
export MOZ_ENABLE_WAYLAND=true

# QT apps
export QT_QPA_PLATFORM=wayland
# Prevent Qt from drawing client side decorations
export QT_WAYLAND_DISABLE_WINDOWDECORATIONS="1"
# Use GTK theme
export QT_QPA_PLATFORMTHEME=qt5ct

# SDL, like imv image viewer
export SDL_VIDEODRIVER=wayland

# Elementary/EFL
export ECORE_EVAS_ENGINE=wayland_egl
export ELM_ENGINE=wayland_egl

# Java applications
export _JAVA_AWT_WM_NONREPARENTING=1

# Cursor theme for apps that don't pick up the gtk settings
# or ~/.icons/default/index.theme.
# Necessary for, e.g. keepassxc
export XCURSOR_THEME=breeze_cursors

# }}}

# PATHS AND SOURCES {{{

# fzf:
[ -f /etc/zsh_completion.d/fzf-key-bindings ] && source /etc/zsh_completion.d/fzf-key-bindings

# PATH (add ruby, yarn and haskell packages)
export PATH="$HOME/bin:$HOME/.cabal/bin:$HOME/.local/bin:$HOME/.gem/ruby/2.6.0/bin:$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"

# load asdf https://asdf-vm.com/#/core-manage-asdf-vm for switching node envs
#. $HOME/.asdf/asdf.sh
#. $HOME/.asdf/completions/asdf.bash

# }}}

# PROMPT {{{

# custom virtualenv prompt
virtualenv_info() {
    [ $VIRTUAL_ENV ] && echo ' ('`basename $VIRTUAL_ENV`')'
}

# custom git prompt
function git_info() {
    local DIRTY="%F{yellow}"
    local CLEAN="%F{green}"
    local UNMERGED="%F{red}"
    git rev-parse --git-dir >& /dev/null
    if [[ $? == 0 ]]; then
        echo -n " "
        if [[ `git ls-files -u >& /dev/null` == '' ]]; then
            git diff --quiet >& /dev/null
            if [[ $? == 1 ]]
            then
                echo -n $DIRTY
            else
                git diff --cached --quiet >& /dev/null
                if [[ $? == 1 ]]; then
                    echo -n $DIRTY
                else
                    echo -n $CLEAN
                fi
            fi
        else
            echo -n $UNMERGED
        fi
        echo -n `git branch | grep '* ' | sed 's/..//'`
    fi
}

# defining prompt here so virtualenv and git info get re-calculated
PROMPT='%F{cyan}%B%2~%b$(virtualenv_info)%f$(git_info) %F{cyan}>%f '

# }}}

# SWAY {{{

# If running from tty1 start sway
if [ "$(tty)" = "/dev/tty1" ]; then
	dbus-launch --exit-with-session sway
	exit 0
fi

# }}}
