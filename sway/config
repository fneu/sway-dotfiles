### Variables
#
# Logo key. Use Mod1 for Alt.
set $mod Mod4
# Home row direction keys, like vim
set $left h
set $down j
set $up k
set $right l
# Your preferred terminal emulator
set $term alacritty

### Output configuration
#
# Wallpaper
output * bg ~/.wallpaper fill

# Example configuration:
#
#   output HDMI-A-1 resolution 1920x1080 position 1920,0
#
# You can get the names of your outputs by running: swaymsg -t get_outputs

# Multi Monitor Desktop "baron"

# external HDMI-monitor of P51
output DP-2 {
    position 1920,0
}

output "Dell Inc. DELL U2412M 0FFXD314401S" {
    resolution 1920x1200
    position 0,0
    bg ~/.wallpaper_left fill
}
output "Unknown L27ADS 0x00000101" {
    resolution 1920x1080
    position 1920,0
}
output "Dell Inc. DELL U2412M 0FFXD31N0PES" {
    resolution 1920x1200
    position 3840,0
    bg ~/.wallpaper_right fill
}

# 4k UHD monitor on P51
output "Unknown 0x21EB 0x00000000" {
    scale 2
    position 0,0
}


### Idle configuration
#
# Example configuration:
#
# exec swayidle -w \
#          timeout 300 'swaylock -f -c 000000' \
#          timeout 600 'swaymsg "output * dpms off"' \
#               resume 'swaymsg "output * dpms on"' \
#          before-sleep 'swaylock -f -c 000000'
#
# This will lock your screen after 300 seconds of inactivity, then turn off
# your displays after another 300 seconds, and turn your screens back on when
# resumed. It will also lock your screen before your computer goes to sleep.

exec swayidle -w \
        before-sleep 'lock'

### Input configuration
#
# Example configuration:
#
#   input "2:14:SynPS/2_Synaptics_TouchPad" {
#       dwt enabled
#       tap enabled
#       natural_scroll enabled
#       middle_emulation enabled
#   }
#
# You can get the names of your inputs by running: swaymsg -t get_inputs
# Read `man 5 sway-input` for more information about this section.

input "*" {
    # Default keyboard layout
    xkb_layout de
    xkb_variant nodeadkeys
    xkb_options caps:escape
    # Default touchpad behaviour
    tap enabled
}

### Visuals and Behaviour
#
# Colors:
#

# class | border | background | text | indicator | childborder
client.focused #3daee9 #2d5c76 #fefefe #1abb9b #3daee9
client.focused_inactive #7f8b8c #313b3c #fbfbfb #7f8b8c #7f8b8c
client.unfocused #63686D #313b3c #bdc3c7 #63686d #63686d

# gaps inner 8
default_border pixel 1

# set GTK theme:
# https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland

set $gnome-schema org.gnome.desktop.interface

exec_always {
    gsettings set $gnome-schema gtk-theme Breeze-Dark
    gsettings set $gnome-schema icon-theme breeze-dark
    gsettings set $gnome-schema cursor-theme breeze_cursors
    gsettings set $gnome-schema font-name "Cantarell 11"
}

set $wp1 1:
set $wp2 2:
set $wp3 3:
set $wp4 4:
set $wp5 5:
set $wp6 6:
set $wp7 7:
set $wp8 8:
set $wp9 9:
set $wp10 10:

# highest priority first
workspace $wp1 output 'Dell Inc. DELL U2412M 0FFXD314401S' eDP-1
workspace $wp2 output 'Dell Inc. DELL U2412M 0FFXD314401S' eDP-1
workspace $wp3 output 'Dell Inc. DELL U2412M 0FFXD314401S' eDP-1
workspace $wp4 output 'Unknown L27ADS 0x00000101' HDMI-A-1 DP-2
workspace $wp5 output 'Unknown L27ADS 0x00000101' HDMI-A-1 DP-2
workspace $wp6 output 'Unknown L27ADS 0x00000101' HDMI-A-1 DP-2
workspace $wp7 output 'Unknown L27ADS 0x00000101' HDMI-A-1 DP-2
workspace $wp8 output 'Dell Inc. DELL U2412M 0FFXD31N0PES' DP-3
workspace $wp9 output 'Dell Inc. DELL U2412M 0FFXD31N0PES' DP-3
workspace $wp10 output 'Dell Inc. DELL U2412M 0FFXD31N0PES' DP-3

# make sure the correct workspaces are shown on multi-monitor setups
workspace $wp8
workspace $wp4
workspace $wp1

### Key bindings
#
# Basics:
#
    # start a terminal
    bindsym $mod+Return exec $term

    # kill focused window
    bindsym $mod+Shift+q kill

    # launchers
    bindsym $mod+Shift+d exec alacritty --class=launcher -e "fzf-launcher-cmd"
    bindsym $mod+d exec alacritty --class=launcher -e "fzf-launcher-desktop"
    bindsym $mod+y exec alacritty --class=launcher -e "fzf-launcher-pass"
    for_window [app_id="^launcher$"] floating enable, resize set 444 250

    # Drag floating windows by holding down $mod and left mouse button.
    # Resize them with right mouse button + $mod.
    # Despite the name, also works for non-floating windows.
    # Change normal to inverse to use left mouse button for resizing and right
    # mouse button for dragging.
    floating_modifier $mod normal

    # reload the configuration file
    bindsym $mod+Shift+c reload

    # exit sway (logs you out of your Wayland session)
    bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

    # toggle bar
    bindsym $mod+t bar mode toggle
#
# Moving around:
#
    # Move your focus around
    bindsym $mod+$left focus left
    bindsym $mod+$down focus down
    bindsym $mod+$up focus up
    bindsym $mod+$right focus right
    # or use $mod+[up|down|left|right]
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right

    # _move_ the focused window with the same, but add Shift
    bindsym $mod+Shift+$left move left
    bindsym $mod+Shift+$down move down
    bindsym $mod+Shift+$up move up
    bindsym $mod+Shift+$right move right
    # ditto, with arrow keys
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right

#
# Workspaces:
#
    # switch to workspace
    bindsym $mod+1 workspace $wp1
    bindsym $mod+2 workspace $wp2
    bindsym $mod+3 workspace $wp3
    bindsym $mod+4 workspace $wp4
    bindsym $mod+5 workspace $wp5
    bindsym $mod+6 workspace $wp6
    bindsym $mod+7 workspace $wp7
    bindsym $mod+8 workspace $wp8
    bindsym $mod+9 workspace $wp9
    bindsym $mod+0 workspace $wp10
    # move focused container to workspace
    bindsym $mod+Shift+1 move container to workspace $wp1
    bindsym $mod+Shift+2 move container to workspace $wp2
    bindsym $mod+Shift+3 move container to workspace $wp3
    bindsym $mod+Shift+4 move container to workspace $wp4
    bindsym $mod+Shift+5 move container to workspace $wp5
    bindsym $mod+Shift+6 move container to workspace $wp6
    bindsym $mod+Shift+7 move container to workspace $wp7
    bindsym $mod+Shift+8 move container to workspace $wp8
    bindsym $mod+Shift+9 move container to workspace $wp9
    bindsym $mod+Shift+0 move container to workspace $wp10
    # move workspaces to adjacent monitors
    bindsym $mod+Control+Shift+$left move workspace to output left
    bindsym $mod+Control+Shift+$down move workspace to output down
    bindsym $mod+Control+Shift+$up move workspace to output up
    bindsym $mod+Control+Shift+$right move workspace to output right
    # ditto, with arrow keys
    bindsym $mod+Control+Shift+Left move workspace to output left
    bindsym $mod+Control+Shift+Down move workspace to output down
    bindsym $mod+Control+Shift+Up move workspace to output up
    bindsym $mod+Control+Shift+Right move workspace to output right

    # Note: workspaces can have any name you want, not just numbers.
    # We just use 1-10 as the default.
#
# Layout stuff:
#
    # You can "split" the current object of your focus with
    # $mod+b or $mod+v, for horizontal and vertical splits
    # respectively.
    bindsym $mod+b splith
    bindsym $mod+v splitv

    # Switch the current container between different layout styles
    bindsym $mod+s layout stacking
    bindsym $mod+w layout tabbed
    bindsym $mod+e layout toggle split

    # Make the current focus fullscreen
    bindsym $mod+f fullscreen

    # Toggle the current focus between tiling and floating mode
    bindsym $mod+Shift+space floating toggle

    # Swap focus between the tiling area and the floating area
    bindsym $mod+space focus mode_toggle

    # move focus to the parent container
    bindsym $mod+a focus parent
#
# Scratchpad:
#
    # Sway has a "scratchpad", which is a bag of holding for windows.
    # You can send windows there and get them back later.

    # Move the currently focused window to the scratchpad
    bindsym $mod+Shift+minus move scratchpad

    # Show the next scratchpad window or hide the focused scratchpad window.
    # If there are multiple scratchpad windows, this command cycles through them.
    bindsym $mod+minus scratchpad show
#
# Resizing containers:
#
mode "resize" {
    # left will shrink the containers width
    # right will grow the containers width
    # up will shrink the containers height
    # down will grow the containers height
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # ditto, with arrow keys
    bindsym Left resize shrink width 10px
    bindsym Down resize grow height 10px
    bindsym Up resize shrink height 10px
    bindsym Right resize grow width 10px

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

#
# Power menu:
#
mode "[l]ock [s]uspend [p]oweroff [r]eboot" {

    bindsym l exec lock; mode "default"
    bindsym s exec systemctl suspend; mode "default"
    bindsym p exec "killall firefox; killall nextcloud; sleep 1; systemctl poweroff"
    bindsym r exec systemctl reboot

    # return to default mode
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+p mode "[l]ock [s]uspend [p]oweroff [r]eboot"

#
# Special keys
#

    bindsym XF86MonBrightnessUp exec ~/.config/sway/util/brightness_inc && ~/.config/sway/refresh
    bindsym XF86MonBrightnessDown exec ~/.config/sway/util/brightness_dec && ~/.config/sway/refresh
    bindsym XF86AudioMute exec pactl set-sink-mute 0 toggle && ~/.config/sway/refresh
    bindsym XF86AudioRaiseVolume exec pactl set-sink-mute 0 false && pactl set-sink-volume 0 +5% && ~/.config/sway/refresh
    bindsym XF86AudioLowerVolume exec pactl set-sink-mute 0 false && pactl set-sink-volume 0 -5% && ~/.config/sway/refresh

    # screenshots
    # copied to clipboard and saved in PICTURES/screenshots
    bindsym Print exec screenshot
    bindsym Shift+Print exec area-screenshot

    # notifications
    bindsym $mod+n exec makoctl dismiss
    bindsym $mod+Shift+n exec makoctl dismiss -a
    bindsym $mod+Control+n exec makoctl invoke

#
# Window rules
#
    for_window [app_id="firefox"] move container to workspace $wp1
    for_window [app_id="evolution"] move container to workspace $wp2
    for_window [app_id="telegram-desktop"] move container to workspace $wp3
    for_window [class="Mattermost"] move container to workspace $wp3
    for_window [class="discord"] move container to workspace $wp7
    for_window [class="zoom"] move container to workspace $wp7
    for_window [class="Skype"] move container to workspace $wp7
    for_window [app_id="org.kde.kbibtex"] move container to workspace $wp8
    for_window [app_id="zim"] move container to workspace $wp9
    for_window [app_id="org.keepassxc.KeePassXC"] move container to workspace $wp10

    for_window [title="^Figure*"] floating enable

#
# Autostart
#

exec firefox
exec evolution
exec telegram-desktop
exec mattermost.AppImage

exec zim
exec keepassxc

exec nextcloud
exec mako

#
# Status Bar:
#
# Read `man 5 sway-bar` for more information about this section.
bar {
    position bottom
    mode dock

    # When the status_command prints a new line to stdout, swaybar updates.
    # The default just shows the current date and time.
    # status_command while date +'%Y-%m-%d %l:%M:%S %p'; do sleep 1; done
    status_command bar-status

    # output command can be specified multiple times
    # NONE: show on all desktops
    # output eDP-1
    # output HDMI-A-1
## class | border | background | text | indicator | childborder
#client.focused #3daee9 #2d5c76 #fefefe #1abb9b #3daee9
#client.focused_inactive #7f8b8c #313b3c #fbfbfb #7f8b8c #7f8b8c
#client.unfocused #63686D #313b3c #bdc3c7 #63686d #63686

    colors {
        statusline #fbfbfb
        background #000000
        separator #313b3c
        focused_workspace #3daee9 #2d5c76 #fefefe
        active_workspace #7f8b8c #313b3c #fbfbfb
        inactive_workspace #313b3c #000000 #bdc3c7
        urgent_workspace #ec1515 #762326 #fefefe
        binding_mode #ec1515 #762326 #fefefe
    }
    icon_theme Papirus
}

include /etc/sway/config.d/*
