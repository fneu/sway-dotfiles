#! /bin/bash

sudo zypper addrepo -f http://packman.inode.at/suse/openSUSE_Tumbleweed/ packman
sudo zypper --gpg-auto-import-keys --non-interactive install --allow-vendor-change \
    ffmpeg lame gstreamer-plugins-bad gstreamer-plugins-ugly \
    gstreamer-plugins-ugly-orig-addon gstreamer-plugins-libav \
    libavdevice56 libavdevice58 libdvdcss2 vlc-codecs
sudo zypper --gpg-auto-import-keys --non-interactive dup --allow-vendor-change \
    --from http://packman.inode.at/suse/openSUSE_Tumbleweed/
